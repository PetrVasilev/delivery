const jwt = require('jsonwebtoken')

const checkAdmin = authorization => {
    if (!authorization) {
        return null
    }
    return jwt.verify(authorization, process.env.ADMIN_SECRET, (err, decoded) => {
        if (err) {
            return null
        }
        return decoded
    })
}

const checkUser = authorization => {
    if (!authorization) {
        return null
    }
    return jwt.verify(authorization, process.env.USER_SECRET, (err, decoded) => {
        if (err) {
            return null
        }
        return decoded
    })
}

module.exports = {
    checkAdmin,
    checkUser
}
