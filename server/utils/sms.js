const axios = require('axios')

module.exports = async ({ smsCode, phone }) => {
    const apiKey = process.env.SMS_KEY

    if (process.env.NODE_ENV === 'production') {
        try {
            const text = `Код подтверждения ${smsCode}`
            await axios.get('https://sms.ru/sms/send', {
                params: {
                    api_id: apiKey,
                    to: phone,
                    msg: text,
                    json: 1
                }
            })
        } catch (err) {
            console.error(err)
        }
    } else {
        console.log('SMS Code', smsCode)
    }
}
