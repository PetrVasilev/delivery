const { Expo } = require('expo-server-sdk')

module.exports = async (tokens = [], message) => {
    let expo = new Expo()

    let messages = []

    for (let pushToken of tokens) {
        if (!Expo.isExpoPushToken(pushToken)) {
            continue
        }
        messages.push({
            to: pushToken,
            sound: 'default',
            body: message
        })
    }

    let chunks = expo.chunkPushNotifications(messages)

    for (let chunk of chunks) {
        try {
            await expo.sendPushNotificationsAsync(chunk)
        } catch (error) {
            console.error(error)
        }
    }
}
