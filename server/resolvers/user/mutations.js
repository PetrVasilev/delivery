const jwt = require('jsonwebtoken')
const randomstring = require('randomstring')

const User = require('../../models/User')
const errors = require('../../utils/errors')
const sendSms = require('../../utils/sms')

module.exports = {
    loginUser: async (_, { data }) => {
        const { phone } = data
        const user = await User.findOne({ phone })
        let SMSCode = null
        if (phone === '79991112233') {
            SMSCode = 1234
        } else {
            SMSCode = randomstring.generate({
                length: 4,
                charset: 'numeric'
            })
            sendSms({
                smsCode: SMSCode,
                phone
            })
        }
        if (!user) {
            const user = new User({
                phone,
                smsCode: SMSCode
            })
            await user.save()
            return true
        } else {
            user.smsCode = SMSCode
            await user.save()
            return true
        }
    },
    confirmLoginUser: async (_, { data }) => {
        const { phone, smsCode } = data
        const user = await User.findOne({ smsCode, phone })
        if (!user) throw new Error(errors.notAccess)
        user.smsCode = 'confirmed'
        const confirmedUser = await user.save()
        const token = jwt.sign({ _id: confirmedUser._id }, process.env.USER_SECRET)
        return {
            token,
            user: confirmedUser
        }
    },
    updateUser: async (_, { data }, { checkUser }) => {
        const { name, address } = data
        const permission = await checkUser()
        if (!permission) throw new Error(errors.notAccess)
        const user = await User.findById(permission._id)
        if (!user) throw new Error(errors.notFound)
        if (name) {
            user.name = name
        }
        if (address) {
            user.address = address
        }
        return await user.save()
    },
    setUserPushToken: async (_, { data }, { checkUser }) => {
        const { token } = data
        const permission = await checkUser()
        if (!permission) throw new Error(errors.notAccess)
        const user = await User.findById(permission._id)
        if (!user) throw new Error(errors.notFound)
        user.pushToken = token
        await user.save()
        return true
    }
}
