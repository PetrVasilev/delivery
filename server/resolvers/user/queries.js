const User = require('../../models/User')

const errors = require('../../utils/errors')

module.exports = {
    _user: async (_, {}, { checkUser }) => {
        const permission = await checkUser()
        if (!permission) throw new Error(errors.notAccess)
        const user = await User.findById(permission._id)
        if (!user) throw new Error(errors.notAccess)
        return user
    },
    users: async (_, { limit, skip }, { checkAdmin }) => {
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)
        const users = await User.find()
            .limit(limit)
            .skip(skip)
        return users
    },
    usersTotal: async (_, {}, { checkAdmin }) => {
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)
        return await User.find().countDocuments()
    }
}
