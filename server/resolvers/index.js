const AdminQueries = require('./admin/queries')
const UserQueries = require('./user/queries')
const OrderQueries = require('./order/queries')

const AdminMutations = require('./admin/mutations')
const UserMutations = require('./user/mutations')
const OrderMutations = require('./order/mutations')

const OrderPopulations = require('./order/populations')

module.exports = {
    Query: {
        ...AdminQueries,
        ...UserQueries,
        ...OrderQueries
    },
    Mutation: {
        ...AdminMutations,
        ...UserMutations,
        ...OrderMutations
    },
    Order: OrderPopulations
}
