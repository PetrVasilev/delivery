const Admin = require('../../models/Admin')
const errors = require('../../utils/errors')

module.exports = {
    admin: async (_, {}, { checkAdmin }) => {
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)
        const admin = await Admin.findById(permission._id)
        if (!admin) throw new Error(errors.notFound)
        return admin
    }
}
