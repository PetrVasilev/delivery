const bcrypt = require('bcryptjs')

const Admin = require('../../models/Admin')
const User = require('../../models/User')
const errors = require('../../utils/errors')
const sendNotification = require('../../utils/notification')

module.exports = {
    loginAdmin: async (_, { data }) => {
        const { login, password } = data
        const log = await Admin.authorize(login, password)
        if (log.authorized) {
            return {
                token: log.token,
                admin: log.admin
            }
        } else {
            throw new Error(errors.authError)
        }
    },
    editPasswordAdmin: async (_, { data }, { checkAdmin }) => {
        const { currentPassword, newPassword } = data
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)
        const admin = await Admin.findById(permission._id)
        if (!admin) throw new Error(errors.notFound)
        const compareState = bcrypt.compareSync(currentPassword, admin.password)
        if (!compareState) throw new Error(errors.authError)
        admin.password = newPassword
        return await admin.save()
    },
    createMailing: async (_, { data }, { checkAdmin }) => {
        const { text } = data
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)
        const users = await User.find()
        const tokens = users.map(item => item.pushToken)
        await sendNotification(tokens, text)
        return true
    }
}
