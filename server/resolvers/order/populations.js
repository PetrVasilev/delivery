const User = require('../../models/User')

module.exports = {
    user: async order => {
        return await User.findById(order.user)
    }
}
