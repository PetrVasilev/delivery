const Order = require('../../models/Order')

module.exports = {
    allOrders: async (_, { skip, limit }, { checkAdmin }) => {
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)
        return await Order.find()
            .skip(skip)
            .limit(limit)
            .sort('-created')
    },
    orders: async (_, { skip, limit }) => {
        return await Order.find({ status: 'active', deleted: false })
            .skip(skip)
            .limit(limit)
            .sort('-created')
    },
    order: async (_, { where }) => {
        return await Order.findById(where._id)
    },
    userOrders: async (_, {}, { checkUser }) => {
        const permission = await checkUser()
        if (!permission) throw new Error(errors.notAccess)
        const orders = await Order.find({ user: permission._id, deleted: false }).sort('-created')
        return orders
    },
    totalOrders: async () => {
        return await Order.find().countDocuments()
    }
}
