const Order = require('../../models/Order')
const User = require('../../models/User')

const errors = require('../../utils/errors')

module.exports = {
    createOrder: async (_, { data }, { checkUser }) => {
        const permission = await checkUser()
        if (!permission) throw new Error(errors.notAccess)
        const user = await User.findById(permission._id)
        if (data.address) {
            user.address = data.address
            await user.save()
        }
        const order = new Order({
            ...data,
            user: permission._id
        })
        return await order.save()
    },
    toggleActiveOrder: async (_, { where }, { checkUser, checkAdmin }) => {
        const [permissionUser, permissionAdmin] = await Promise.all([checkUser(), checkAdmin()])
        if (!permissionUser && !permissionAdmin) throw new Error(errors.notAccess)
        const order = await Order.findById(where._id)
        if (!order) throw new Error(errors.notFound)
        const isActive = order.status === 'active'
        if (!isActive) {
            order.created = Date.now()
        }
        order.status = isActive ? 'inactive' : 'active'
        return await order.save()
    },
    deleteOrder: async (_, { where }, { checkUser }) => {
        const permission = await checkUser()
        if (!permission) throw new Error(errors.notAccess)

        const order = await Order.findById(where._id)
        if (!order) throw new Error(errors.notFound)
        order.deleted = true
        order.status = 'inactive'
        return await order.save()
    },
    deleteOrderAdmin: async (_, { where }, { checkAdmin }) => {
        const permission = await checkAdmin()
        if (!permission) throw new Error(errors.notAccess)

        const order = await Order.findById(where._id)
        if (!order) throw new Error(errors.notFound)

        return await order.remove()
    }
}
