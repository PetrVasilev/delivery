// const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const { GraphQLServer } = require('graphql-yoga')

const resolvers = require('./resolvers')
const Admin = require('./models/Admin')
const { checkAdmin, checkUser } = require('./utils/auth')

dotenv.config()

const mongoURL = process.env.MONGO_URL
mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true })
mongoose.set('useCreateIndex', true)
mongoose.set('useFindAndModify', false)
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Mongo connection ERROR :('))
db.once('open', () => {
    console.log(`Mongo connected to ${mongoURL} :)`)
})

const isProduction = process.env.NODE_ENV === 'production'

const server = new GraphQLServer({
    typeDefs: 'schemas/index.graphql',
    resolvers,
    context: ({ request }) => {
        const { authorization } = request.headers
        return {
            checkAdmin: () => {
                return checkAdmin(authorization)
            },
            checkUser: () => {
                return checkUser(authorization)
            }
        }
    }
})
const port = process.env.PORT

server.express.use((_, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

// server.express.use('/uploads', express.static('../uploads'))

server.start(
    {
        port,
        endpoint: '/graphql',
        playground: '/playground',
        bodyParserOptions: { limit: '10mb', type: 'application/json' },
        debug: isProduction ? false : true
    },
    () => {
        Admin.initDB()
        console.log(`Server is running on port:${port}`)
    }
)
