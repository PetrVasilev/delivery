const mongoose = require('mongoose')

const { Schema } = mongoose
const { ObjectId } = Schema.Types

const schema = new Schema({
    address: { type: String },
    products: [{ type: String }],
    user: { type: ObjectId, ref: 'User' },
    comment: { type: String },
    whatsapp: { type: Boolean, default: false },
    created: { type: Date, default: Date.now },
    status: { type: String, default: 'active' }, // active, inactive
    deleted: { type: Boolean, default: false }
})

module.exports = mongoose.model('Order', schema)
