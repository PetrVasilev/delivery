const mongoose = require('mongoose')

const { Schema } = mongoose

const schema = new Schema({
    name: { type: String },
    phone: { type: String },
    address: { type: String },
    smsCode: { type: String },
    created: { type: Date, default: Date.now },
    pushToken: { type: String }
})

module.exports = mongoose.model('User', schema)
