const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const { Schema } = mongoose

const schema = new Schema({
    login: { type: String, required: true, unique: true },
    password: { type: String },
    name: { type: String }
})

schema.pre('save', function(next) {
    if (!this.isModified('password')) {
        return next()
    }
    bcrypt.hash(this.password, 10, (_, hash) => {
        this.password = hash
        return next()
    })
})

schema.statics.authorize = async function(login, password) {
    const Admin = this
    try {
        const admin = await Admin.findOne({ login }).exec()
        if (!admin) {
            return { authorized: false }
        }
        const compareState = bcrypt.compareSync(password, admin.password)
        if (!compareState) {
            return { authorized: false }
        } else {
            const token = jwt.sign({ _id: admin._id }, process.env.ADMIN_SECRET)
            return { authorized: true, admin, token }
        }
    } catch (err) {
        console.error(err)
        return { authorized: false }
    }
}

schema.statics.initDB = async function() {
    const Admin = this
    const login = process.env.ADMIN_LOGIN
    const password = process.env.ADMIN_PASSWORD
    try {
        const admin = await Admin.findOne({ login })
        if (!admin) {
            const newAdmin = new Admin({
                login,
                name: 'Super Admin',
                password
            })
            await newAdmin.save()
            console.log(`Super Admin created :)`)
        }
    } catch (err) {
        console.error(err)
    }
}

module.exports = mongoose.model('Admin', schema)
