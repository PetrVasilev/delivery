import gql from 'graphql-tag'

export const LOGIN_USER = gql`
    mutation($phone: String!) {
        loginUser(data: { phone: $phone })
    }
`

export const CONFIRM_LOGIN_USER = gql`
    mutation($phone: String!, $smsCode: String!) {
        confirmLoginUser(data: { phone: $phone, smsCode: $smsCode }) {
            token
            user {
                _id
                name
                phone
                address
                created
            }
        }
    }
`

export const UPDATE_USER = gql`
    mutation($name: String, $address: String) {
        updateUser(data: { name: $name, address: $address }) {
            _id
            name
            phone
            address
            created
        }
    }
`

export const SET_PUSH_TOKEN = gql`
    mutation($token: String!) {
        setUserPushToken(data: { token: $token })
    }
`
