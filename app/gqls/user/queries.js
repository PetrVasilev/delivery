import gql from 'graphql-tag'

export const GET_USER = gql`
    {
        _user {
            _id
            name
            phone
            address
            created
        }
    }
`

export const GET_USERS_ORDERS = gql`
    {
        userOrders {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            status
            whatsapp
            created
            deleted
        }
    }
`
