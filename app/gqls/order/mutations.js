import gql from 'graphql-tag'

export const CREATE_ORDER = gql`
    mutation($address: String!, $products: [String!]!, $comment: String, $whatsapp: Boolean) {
        createOrder(
            data: { address: $address, products: $products, comment: $comment, whatsapp: $whatsapp }
        ) {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            whatsapp
            created
            status
            deleted
        }
    }
`

export const TOGGLE_ACTIVE_ORDER = gql`
    mutation($_id: String!) {
        toggleActiveOrder(where: { _id: $_id }) {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            whatsapp
            created
            status
            deleted
        }
    }
`

export const DELETE_ORDER = gql`
    mutation($_id: String!) {
        deleteOrder(where: { _id: $_id }) {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            whatsapp
            created
            status
            deleted
        }
    }
`
