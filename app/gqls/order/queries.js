import gql from 'graphql-tag'

export const GET_ORDERS = gql`
    query($limit: Int, $skip: Int) {
        orders(limit: $limit, skip: $skip) {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            whatsapp
            created
            status
            deleted
        }
    }
`
