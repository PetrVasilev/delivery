import React from 'react'
import styled from 'styled-components'

const View = styled.View`
    width: 100%;
    border: 1px solid ${props => (props.error ? '#ed4337' : '#d4d4d4')};
    border-left-width: 0;
    border-right-width: 0;
    padding: 0 15px;
    height: 46px;
    background-color: white;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
`

const Switch = styled.Switch``

const Label = styled.Text`
    font-size: 16px;
    color: black;
`

const Checkbox = ({ label, checked = false, toggleCheck, ...props }) => {
    return (
        <View>
            <Label>{label}</Label>
            <Switch {...props} onValueChange={toggleCheck} value={checked} />
        </View>
    )
}

export default Checkbox
