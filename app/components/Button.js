import React from 'react'
import { ActivityIndicator } from 'react-native'
import styled from 'styled-components/native'
import { Ionicons } from '@expo/vector-icons'

import colors from '../utils/colors'

const View = styled.TouchableOpacity`
    background-color: ${colors.primary};
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 46px;
`

const Text = styled.Text`
    font-size: 16px;
    color: white;
    text-align: center;
`

const WithIcon = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
`

const Button = ({ children = 'Баттон', icon, labelColor, loading, ...props }) => {
    return (
        <View {...props} disabled={loading}>
            {loading ? (
                <ActivityIndicator color="white" />
            ) : icon ? (
                <WithIcon>
                    <Text>{children}</Text>
                    <Ionicons name={icon} color="white" size={20} style={{ marginLeft: 8 }} />
                </WithIcon>
            ) : (
                <Text>{children}</Text>
            )}
        </View>
    )
}

export default Button
