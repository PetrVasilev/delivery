import React from 'react'
import styled from 'styled-components/native'

const View = styled.View`
    width: 100%;
    margin-bottom: 15px;
`

const RNTextInput = styled.TextInput`
    width: 100%;
    border: 1px solid ${props => (props.error ? '#ed4337' : '#d4d4d4')};
    border-left-width: 0;
    border-right-width: 0;
    padding: 13px 15px;
    font-size: 16px;
    background-color: white;
    height: 46px;
`

const Label = styled.Text`
    font-size: 14px;
    margin-bottom: 6px;
    padding-left: 15px;
`

const Error = styled.Text`
    font-size: 12px;
    color: red;
    margin-top: 5px;
    padding-left: 15px;
`

const TextInputContainer = styled.View`
    position: relative;
`

const Action = styled.View`
    position: absolute;
    height: 100%;
    right: 0;
`

const clearMaskedValue = (maskType, value) => {
    return maskType === 'phone' ? value.replace(/\D/g, '') : value
}

const isMasked = (maskType, value) => {
    switch (maskType) {
        case 'phone':
            return phoneMask(value)
        default:
            return value
    }
}

const setMaxLength = (maskType, maxLength) => {
    switch (maskType) {
        case 'phone':
            return 18
        default:
            return maxLength
    }
}

export const phoneMask = value => {
    if (value) {
        if (value.match(/^(\+7|7|8)+/g)) {
            let arr = value
                .replace(/^(\+7|7|8)+/g, '+7')
                .match(/^(\+7|7|8)?(\d{1,3})?(\d{1,3})?(\d{1,2})?(\d{1,2})?/)
            let res = ''
            for (let i = 1; i < 6; i++) {
                let item = arr[i]
                if (item) {
                    if (i == 2) {
                        res = res + ' ' + `(${item}`
                    } else if (i == 3) {
                        res = res + ') ' + item
                    } else {
                        res = res + ' ' + item
                    }
                }
            }
            return res.replace(/^ /, '')
        } else {
            return '+7' + value
        }
    } else {
        return value
    }
}

const TextInput = ({
    label,
    containerStyle,
    mask,
    value,
    error,
    onChangeText,
    maxLength,
    action,
    ...props
}) => {
    const handleChange = value => {
        const clearedValue = clearMaskedValue(mask, value)
        if (onChangeText) {
            onChangeText(clearedValue)
        }
    }

    return (
        <View style={containerStyle}>
            {label ? <Label>{label}</Label> : null}
            <TextInputContainer>
                <RNTextInput
                    maxLength={setMaxLength(mask, maxLength)}
                    value={isMasked(mask, value)}
                    onChangeText={handleChange}
                    error={error}
                    {...props}
                />
                {action ? <Action>{action}</Action> : null}
            </TextInputContainer>
            {error ? <Error>{error}</Error> : null}
        </View>
    )
}

export default TextInput
