import React, { useRef } from 'react'
import Toast from 'react-native-easy-toast'

let toast = {}

export const showToast = text => {
    toast.show(text, 2000)
}

const ToastComponent = () => {
    toast = useRef()

    return (
        <Toast
            ref={c => (toast = c)}
            positionValue={60}
            fadeInDuration={200}
            fadeOutDuration={300}
            position="top"
            opacity={0.8}
            style={{
                maxWidth: '80%'
            }}
        />
    )
}

export default ToastComponent
