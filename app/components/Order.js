import React from 'react'
import styled from 'styled-components/native'
import moment from 'moment'
import 'moment/locale/ru'

import colors from '../utils/colors'

const OrderView = styled.TouchableOpacity`
    width: 100%;
    border: 1px solid ${(props) => (props.error ? '#ed4337' : '#d4d4d4')};
    border-left-width: 0;
    border-right-width: 0;
    padding: 15px;
    background-color: white;
`

const Address = styled.Text`
    font-size: 16px;
    font-weight: 600;
    color: ${colors.primary};
`

const UserName = styled.Text`
    font-size: 14px;
    margin-top: 6px;
`

const Products = styled.Text`
    margin-top: 6px;
    color: gray;
`

const Top = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`

const Created = styled.Text`
    font-size: 12px;
    max-width: 30%;
`

const Status = styled.Text`
    font-size: 14px;
    margin-top: 10px;
    color: ${(props) => (props.status === 'active' ? '#22bb33' : 'red')};
`

const Order = ({ fromProfile = false, order, index, navigation }) => {
    return (
        <OrderView
            onPress={() => navigation.navigate('SingleOrder', { order, fromProfile })}
            style={{ borderTopWidth: index === 0 ? 1 : 0 }}
        >
            <Top>
                <Address>{order.address}</Address>
                <Created numberOfLines={1}>{moment(order.created).fromNow()}</Created>
            </Top>
            <UserName>{order.user.name}</UserName>
            <Products numberOfLines={2}>
                {order.products.length > 1 ? order.products.join(', ') : order.products[0]}
            </Products>
            {fromProfile ? (
                <Status status={order.status}>
                    {order.status === 'active' ? 'Активный' : 'Завершен'}
                </Status>
            ) : null}
        </OrderView>
    )
}

export default Order
