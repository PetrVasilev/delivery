import React from 'react'
import styled from 'styled-components'

const View = styled.View`
    border: 1px solid #d4d4d4;
    border-left-width: 0;
    border-right-width: 0;
    border-bottom-width: 0;
    ${props => (props.last ? `border-bottom-width: 1px;` : '')}
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    flex-direction: row;
    background-color: white;
    padding: 10px 15px;
`

const Label = styled.Text`
    font-size: 16px;
    color: gray;
    margin-bottom: 5px;
`

const Value = styled.Text`
    font-size: 16px;
    color: black;
`

const ValueView = ({ label, value, last }) => {
    return (
        <View last={last}>
            {label ? <Label>{label}</Label> : null}
            <Value style={{ marginBottom: !label ? 5 : null }}>{value}</Value>
        </View>
    )
}

export default ValueView
