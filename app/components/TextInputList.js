import React from 'react'
import { Ionicons } from '@expo/vector-icons'
import styled from 'styled-components/native'

import colors from '../utils/colors'
import TextInput from './TextInput'

const Action = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    padding-right: 15px;
    height: 100%;
    padding-top: 2px;
`

const Actions = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    height: 100%;
`

const TextInputList = ({ label, placeholder, values, setValues }) => {
    return values.map((_, index) => {
        const isLast = values.length - 1 === index
        return (
            <TextInput
                value={values[index]}
                onChangeText={value => {
                    const arr = values.map((item, ind) => {
                        if (ind === index) return value
                        return item
                    })
                    setValues(arr)
                }}
                key={index}
                label={index === 0 ? label : null}
                placeholder={placeholder}
                style={{
                    paddingRight: isLast && values.length > 1 ? 68 : 40,
                    borderTopWidth: index !== 0 ? 0 : 1
                }}
                onSubmitEditing={() => {
                    setValues(prev => [...prev, ''])
                }}
                autoFocus={index !== 0}
                containerStyle={{ marginBottom: isLast ? 15 : 0 }}
                action={
                    isLast ? (
                        <Actions>
                            <Action
                                onPress={() => {
                                    setValues(prev => [...prev, ''])
                                }}
                            >
                                <Ionicons name={'ios-add'} size={28} color={colors.primary} />
                            </Action>
                            {values.length > 1 && (
                                <Action
                                    onPress={() => {
                                        setValues(prev => prev.filter((_, ind) => ind !== index))
                                    }}
                                >
                                    <Ionicons name={'ios-close'} size={32} color={'#ed4337'} />
                                </Action>
                            )}
                        </Actions>
                    ) : (
                        <Action
                            onPress={() => {
                                setValues(prev => prev.filter((_, ind) => ind !== index))
                            }}
                        >
                            <Ionicons name={'ios-close'} size={34} color={'#ed4337'} />
                        </Action>
                    )
                }
            />
        )
    })
}

export default TextInputList
