import React, { useLayoutEffect } from 'react'
import { Alert, AsyncStorage, RefreshControl } from 'react-native'
import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'
import { Ionicons } from '@expo/vector-icons'
import moment from 'moment'
import 'moment/locale/ru'

import { GET_USER, GET_USERS_ORDERS } from '../gqls/user/queries'
import Order from '../components/Order'
import ValueView from '../components/ValueView'
import { phoneMask } from '../components/TextInput'
import colors from '../utils/colors'

const View = styled.ScrollView`
    flex: 1;
    padding: 15px 0;
`

const LogoutButton = styled.TouchableOpacity`
    padding-right: 15px;
`

const BlockLabel = styled.Text`
    font-size: 16px;
    padding-left: 15px;
    margin-bottom: 7px;
    color: gray;
    ${props => (props.top ? `margin-top: ${props.top}px;` : '')}
`

const Orders = styled.View`
    margin-bottom: 30px;
`

const Profile = ({ navigation }) => {
    const { data: userData, client, refetch: userRefetch, loading: userLoading } = useQuery(
        GET_USER
    )
    const { data: ordersData, refetch: ordersRefetch, loading: ordersLoading } = useQuery(
        GET_USERS_ORDERS
    )

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <LogoutButton onPress={askLogout}>
                    <Ionicons name="ios-log-out" color={colors.primary} size={27} />
                </LogoutButton>
            )
        })
    }, [navigation])

    const askLogout = () => {
        Alert.alert('Точно выйти?', null, [
            {
                text: 'Нет'
            },
            {
                text: 'Выйти',
                onPress: logout
            }
        ])
    }

    const logout = async () => {
        AsyncStorage.removeItem('token')
            .then(() => {
                client.writeQuery({
                    query: GET_USER,
                    data: {
                        _user: null
                    }
                })
            })
            .catch(err => console.log(err))
    }

    const user = userData ? (userData._user ? userData._user : {}) : {}
    const orders = ordersData
        ? ordersData.userOrders
            ? ordersData.userOrders.filter(item => !item.deleted)
            : []
        : []

    return (
        <View
            refreshControl={
                <RefreshControl
                    refreshing={ordersLoading || userLoading}
                    onRefresh={async () => {
                        await userRefetch()
                        await ordersRefetch()
                    }}
                />
            }
        >
            <ValueView label="Имя" value={user.name} />
            <ValueView label="Номер телефона" value={phoneMask(user.phone)} />
            <ValueView label="Адрес" value={user.address} />
            <ValueView
                last
                label="Дата регистрации"
                value={moment(user.created).format('DD.MM.YYYY HH:mm')}
            />

            {orders.length > 0 ? (
                <>
                    <BlockLabel top={20}>Твои заказы</BlockLabel>
                    <Orders>
                        {orders.map((item, index) => (
                            <Order
                                fromProfile
                                navigation={navigation}
                                order={item}
                                key={item._id}
                                index={index}
                            />
                        ))}
                    </Orders>
                </>
            ) : null}
        </View>
    )
}

export default Profile
