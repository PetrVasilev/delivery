import React, { useState } from 'react'
import {
    TouchableWithoutFeedback,
    Keyboard,
    Dimensions,
    AsyncStorage,
    Platform
} from 'react-native'
import styled from 'styled-components/native'
import { useMutation, useApolloClient, useLazyQuery } from '@apollo/react-hooks'

import { showToast } from '../components/Toast'
import TextInput from '../components/TextInput'
import Button from '../components/Button'
import colors from '../utils/colors'
import { LOGIN_USER, CONFIRM_LOGIN_USER } from '../gqls/user/mutations'
import { GET_USER } from '../gqls/user/queries'

const { width } = Dimensions.get('window')

const View = styled.View`
    flex: 1;
    padding: ${Platform.OS === 'ios' ? `30px` : `45px`} 0;
`

const LogoView = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
`

const Logo = styled.Image`
    height: 40px;
    width: 40px;
`

const Title = styled.Text`
    font-size: 35px;
    font-style: italic;
    color: ${colors.primary};
    text-align: center;
    letter-spacing: 3px;
    margin-left: 5px;
`

const SubTitle = styled.Text`
    font-size: 18px;
    margin-top: 10px;
    text-align: center;
    color: gray;
`

const Form = styled.View`
    margin-top: 25px;
`

const StyledButton = styled(Button)`
    margin-top: 8px;
`

const ResendButton = styled.TouchableOpacity`
    margin-top: 18px;
`

const ResendText = styled.Text`
    font-size: 16px;
    text-align: center;
    color: ${(props) => (props.active ? colors.primary : 'silver')};
`

const Padding = styled.View`
    padding: 0 15px;
`

const initValue = {
    value: '',
    error: null
}

const resendIntervalTime = 30

const Login = ({ navigation }) => {
    const [phone, setPhone] = useState(initValue)
    const [smsCode, setSmsCode] = useState(initValue)
    const [smsSent, setSmsSent] = useState(false)
    const [resendTime, setResendTime] = useState(resendIntervalTime)

    const client = useApolloClient()
    const [loginUser, { loading: loginLoading }] = useMutation(LOGIN_USER, {
        onCompleted: () => {
            setSmsSent(true)
            const resendInterval = setInterval(() => {
                setResendTime((prev) => {
                    const time = prev - 1
                    if (time === 0) {
                        clearInterval(resendInterval)
                    }
                    return time
                })
            }, 1000)
        },
        onError: (err) => {
            console.log(err.message)
            showToast('Не удалось отправить SMS код. Попробуй еще раз :)')
        }
    })
    const [confirmLogin, { loading: confirmLoading }] = useMutation(CONFIRM_LOGIN_USER, {
        onCompleted: async ({ confirmLoginUser: data }) => {
            const token = data.token
            const user = data.user
            await AsyncStorage.setItem('token', token)
            if (!user.name) {
                navigation.navigate('SetUserInfo')
            } else {
                setUser(user)
            }
        },
        onError: (err) => {
            console.log(err.message)
            showToast('Не правильный SMS код :(')
        }
    })

    const setUser = (user) => {
        client.writeQuery({
            query: GET_USER,
            data: {
                _user: user
            }
        })
    }

    const handleLogin = () => {
        if (phone.value.length === 11) {
            setResendTime(resendIntervalTime)
            return loginUser({
                variables: {
                    phone: phone.value
                }
            })
        }
        return setPhone((prev) => ({
            ...prev,
            error: 'Введи правильный номер телефона'
        }))
    }
    const handleConfirm = () => {
        if (smsCode.value.length !== 4) {
            return setSmsCode((prev) => ({
                ...prev,
                error: 'SMS код должен состоять из 4 цифр'
            }))
        }
        if (phone.value.length !== 11) {
            return setPhone((prev) => ({
                ...prev,
                error: 'Введи правильный номер телефона'
            }))
        }
        return confirmLogin({
            variables: {
                phone: phone.value,
                smsCode: smsCode.value
            }
        })
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View>
                <LogoView>
                    <Logo source={require('../assets/logo.png')} />
                    <Title>Agal</Title>
                </LogoView>
                <SubTitle>Доставка продуктов</SubTitle>
                <Form>
                    <TextInput
                        mask="phone"
                        value={phone.value}
                        onChangeText={(value) => setPhone({ value, error: null })}
                        error={phone.error}
                        placeholder="+7 (9XX) XXX XX XX"
                        label="Номер телефона"
                        keyboardType="phone-pad"
                    />
                    {smsSent ? (
                        <TextInput
                            value={smsCode.value}
                            onChangeText={(value) => setSmsCode({ value, error: null })}
                            error={smsCode.error}
                            placeholder="Код из SMS для подтверждения"
                            label="SMS код"
                            keyboardType={width > 350 ? 'numeric' : 'numbers-and-punctuation'}
                        />
                    ) : null}

                    {smsSent ? (
                        <>
                            <StyledButton
                                onPress={handleConfirm}
                                disabled={confirmLoading}
                                loading={confirmLoading}
                            >
                                Подтвердить SMS код
                            </StyledButton>
                            <ResendButton onPress={handleLogin} disabled={resendTime !== 0}>
                                <ResendText active={resendTime === 0}>
                                    Отправить SMS заново
                                    {resendTime !== 0 ? ` (${resendTime} сек)` : ''}
                                </ResendText>
                            </ResendButton>
                        </>
                    ) : (
                        <StyledButton
                            onPress={handleLogin}
                            disabled={loginLoading}
                            loading={loginLoading}
                        >
                            Отправить SMS код
                        </StyledButton>
                    )}
                </Form>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default Login
