import React, { useState } from 'react'
import { TouchableWithoutFeedback, Keyboard } from 'react-native'
import styled from 'styled-components/native'
import { useMutation, useApolloClient } from '@apollo/react-hooks'

import { showToast } from '../components/Toast'
import TextInput from '../components/TextInput'
import Button from '../components/Button'
import colors from '../utils/colors'
import { UPDATE_USER } from '../gqls/user/mutations'
import { GET_USER } from '../gqls/user/queries'

const View = styled.View`
    flex: 1;
    padding: 30px 0;
`

const LogoView = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
`

const Logo = styled.Image`
    height: 40px;
    width: 40px;
`

const Title = styled.Text`
    font-size: 35px;
    font-style: italic;
    color: ${colors.primary};
    text-align: center;
    letter-spacing: 3px;
    margin-left: 5px;
`

const SubTitle = styled.Text`
    font-size: 18px;
    margin-top: 10px;
    text-align: center;
    color: gray;
`

const Form = styled.View`
    margin-top: 25px;
`

const StyledButton = styled(Button)`
    margin-top: 8px;
`

const initValue = {
    value: '',
    error: null
}

const SetUserInfo = () => {
    const [name, setName] = useState(initValue)
    const [address, setAddress] = useState(initValue)
    const client = useApolloClient()

    const [updateUser, { loading: updateLoading }] = useMutation(UPDATE_USER, {
        onCompleted: ({ updateUser }) => {
            const user = updateUser
            setUser(user)
        },
        onError: err => {
            console.log(err.message)
            showToast('Не удалось выполнить запрос. Попробуй еще раз')
        }
    })

    const setUser = user => {
        client.writeQuery({
            query: GET_USER,
            data: {
                _user: user
            }
        })
    }

    const handleUpdateUser = () => {
        if (!name.value) {
            return setName(prev => ({
                ...prev,
                error: 'Обязательно введи свое имя'
            }))
        }
        return updateUser({
            variables: {
                name: name.value,
                address: address.value
            }
        })
    }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View>
                <LogoView>
                    <Logo source={require('../assets/logo.png')} />
                    <Title>Agal</Title>
                </LogoView>
                <SubTitle>Добавь данные для регистрации</SubTitle>

                <Form>
                    <TextInput
                        value={name.value}
                        onChangeText={value => setName({ value, error: null })}
                        error={name.error}
                        placeholder="Имя Фамилия"
                        label="Имя"
                        autoFocus
                        autoCapitalize="words"
                    />
                    <TextInput
                        value={address.value}
                        onChangeText={value => setAddress({ value, error: null })}
                        error={address.error}
                        placeholder="Улица, номер дома, квартира, подъезд"
                        label="Адрес доставки"
                    />
                    <StyledButton onPress={handleUpdateUser} loading={updateLoading}>
                        Сохранить и Войти
                    </StyledButton>
                </Form>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default SetUserInfo
