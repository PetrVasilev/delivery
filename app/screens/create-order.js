import React, { useState, useEffect } from 'react'
import { KeyboardAvoidingView, Platform } from 'react-native'
import styled from 'styled-components/native'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { Ionicons } from '@expo/vector-icons'

import Button from '../components/Button'
import TextInput from '../components/TextInput'
import Checkbox from '../components/Checkbox'
import TextInputList from '../components/TextInputList'
import { showToast } from '../components/Toast'
import { GET_USER, GET_USERS_ORDERS } from '../gqls/user/queries'
import { SET_PUSH_TOKEN } from '../gqls/user/mutations'
import { CREATE_ORDER } from '../gqls/order/mutations'
import registerForPushNotificationsAsync from '../utils/notification'

const Container = styled.ScrollView`
    padding: 15px 0;
`

const CenteredContainer = styled.View`
    padding: 15px 0;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex: 1;
`

const TextArea = styled(TextInput)`
    min-height: 90px;
    height: auto;
`

const StyledButton = styled(Button)`
    margin-top: 15px;
    margin-bottom: 30px;
`

const SuccessText = styled.Text`
    font-size: 16px;
    color: gray;
    text-align: center;
    margin-top: 0px;
    margin-bottom: 18px;
    padding: 0 30px;
`

const CreateOrder = () => {
    const { data, client } = useQuery(GET_USER)
    const user = data ? (data._user ? data._user : {}) : {}

    const [address, setAddress] = useState({
        value: user.address,
        error: ''
    })
    const [comment, setComment] = useState('')
    const [products, setProducts] = useState([''])
    const [whatsapp, setWhatsapp] = useState(false)
    const [orderCreated, setOrderCreated] = useState(false)

    const [createOrder, { loading: creating }] = useMutation(CREATE_ORDER, {
        onCompleted: ({ createOrder: data }) => {
            try {
                const userOrdersData = client.readQuery({
                    query: GET_USERS_ORDERS
                })
                client.writeQuery({
                    query: GET_USERS_ORDERS,
                    data: {
                        userOrders: [...userOrdersData.userOrders, data]
                    }
                })
            } catch (err) {
                console.log(err)
                client.writeQuery({
                    query: GET_USERS_ORDERS,
                    data: {
                        userOrders: [data]
                    }
                })
            }
            setOrderCreated(true)
        },
        onError: err => {
            console.log(err)
            showToast('Не удалось создать заказ :( Попробуй еще раз')
        }
    })
    const [setPushToken] = useMutation(SET_PUSH_TOKEN)

    const handlePushNotification = async () => {
        const token = await registerForPushNotificationsAsync()
        if (token) {
            setPushToken({
                variables: { token }
            })
        }
    }

    useEffect(() => {
        handlePushNotification()
    }, [])

    const handleCreateOrder = () => {
        const data = {
            address: address.value,
            comment,
            products: products.filter(item => item),
            whatsapp
        }
        if (!data.address) {
            return setAddress(prev => ({
                ...prev,
                error: 'Обязательно нужно указать адрес'
            }))
        }
        if (data.products.length === 0) {
            return showToast('Надо указать хотя бы один продукт')
        }
        return createOrder({
            variables: data
        })
    }

    if (orderCreated) {
        return (
            <CenteredContainer>
                <Ionicons name={'ios-checkmark-circle-outline'} size={100} color={'#22bb33'} />
                <SuccessText>Заказ успешно создан. Ожидайте звонка от доставщиков</SuccessText>
                <Button
                    onPress={() => {
                        setComment('')
                        setProducts([''])
                        setAddress({
                            value: user.address,
                            error: ''
                        })
                        setWhatsapp(false)
                        setOrderCreated(false)
                    }}
                >
                    Заказать еще
                </Button>
            </CenteredContainer>
        )
    }

    return (
        <KeyboardAvoidingView
            style={{ flex: 1 }}
            behavior="padding"
            enabled={Platform.OS === 'ios'}
        >
            <Container>
                <TextInput
                    value={address.value}
                    onChangeText={value =>
                        setAddress({
                            value,
                            error: ''
                        })
                    }
                    error={address.error}
                    label="Адрес"
                    placeholder="Улица, номер дома, квартира, подъезд"
                />
                <TextInputList
                    label="Продукты"
                    placeholder="Название, количество"
                    values={products}
                    setValues={setProducts}
                />
                <TextArea
                    label="Комментарий"
                    placeholder={`Дополнительная информация`}
                    multiline={true}
                    textAlignVertical="top"
                    numberOfLines={4}
                    value={comment}
                    onChangeText={setComment}
                />
                <Checkbox
                    checked={whatsapp}
                    toggleCheck={() => setWhatsapp(prev => !prev)}
                    label="Есть WhatsApp"
                />
                <StyledButton loading={creating} onPress={handleCreateOrder}>
                    Создать заказ
                </StyledButton>
            </Container>
        </KeyboardAvoidingView>
    )
}

export default CreateOrder
