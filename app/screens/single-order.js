import React from 'react'
import { Alert } from 'react-native'
import styled from 'styled-components'
import { Linking } from 'expo'
import { useMutation } from '@apollo/react-hooks'
import moment from 'moment'
import 'moment/locale/ru'

import Button from '../components/Button'
import ValueView from '../components/ValueView'
import { showToast } from '../components/Toast'
import { TOGGLE_ACTIVE_ORDER, DELETE_ORDER } from '../gqls/order/mutations'

const View = styled.ScrollView`
    flex: 1;
    padding: 15px 0;
`

const BlockLabel = styled.Text`
    font-size: 16px;
    padding-left: 15px;
    margin-bottom: 7px;
    color: gray;
    ${props => (props.top ? `margin-top: ${props.top}px;` : '')}
`

const StyledButton = styled(Button)`
    ${props =>
        props.bottom
            ? `
                margin-bottom: 40px;
                margin-top: 10px;
            `
            : `margin-top: 20px;
`}

    ${props => (props.color ? `background-color: ${props.color};` : '')}
`

const SingleOrder = ({ route, navigation }) => {
    const { order, fromProfile } = route.params

    const [toggleActiveMutation, { loading: toggling }] = useMutation(TOGGLE_ACTIVE_ORDER, {
        onCompleted: ({ toggleActiveOrder }) => {
            if (toggleActiveOrder.status === 'active') {
                showToast('Заказ отправлен')
            } else {
                showToast('Заказ завершен')
            }
            navigation.goBack()
        },
        onError: err => {
            console.log(err)
            showToast('Не удалось завершить заказ :( Попробуй заново')
        }
    })
    const [deleteOrderMutation, { loading: deleting }] = useMutation(DELETE_ORDER, {
        onCompleted: () => {
            showToast('Заказ удален')
            navigation.goBack()
        },
        onError: err => {
            console.log(err)
            showToast('Не удалось удалить заказ :( Попробуй заново')
        }
    })

    const toggleActive = () => {
        toggleActiveMutation({
            variables: {
                _id: order._id
            }
        })
    }

    const deleteOrder = () => {
        deleteOrderMutation({
            variables: {
                _id: order._id
            }
        })
    }

    const handleToggleActive = () => {
        Alert.alert('Завершить заказ?', null, [
            {
                text: 'Нет'
            },
            {
                text: 'Да',
                onPress: toggleActive
            }
        ])
    }

    const handleDelete = () => {
        Alert.alert('Удалить заказ?', null, [
            {
                text: 'Нет'
            },
            {
                text: 'Да',
                onPress: deleteOrder
            }
        ])
    }

    return (
        <View>
            <BlockLabel>Информация</BlockLabel>
            <ValueView label={'Пользователь'} value={order.user.name} />
            <ValueView label={'Адрес'} value={order.address} />
            {fromProfile ? (
                <ValueView
                    label={'Статус'}
                    value={order.status === 'active' ? 'Активный' : 'Завершен'}
                />
            ) : null}
            <ValueView
                label={'Время заказа'}
                value={moment(order.created).format('lll')}
                last={!order.comment ? true : false}
            />

            {order.comment ? <ValueView label={'Комментарий'} value={order.comment} last /> : null}

            <BlockLabel top={20}>Товары</BlockLabel>
            {order.products.map((item, index) => (
                <ValueView value={item} last={index === order.products.length - 1} key={index} />
            ))}

            {!fromProfile ? (
                <StyledButton
                    onPress={() => Linking.openURL(`tel:${order.user.phone}`)}
                    icon={'ios-call'}
                    style={{ marginBottom: !order.whatsapp ? 40 : null }}
                >
                    Позвонить
                </StyledButton>
            ) : null}
            {!fromProfile ? (
                order.whatsapp ? (
                    <StyledButton
                        onPress={() => Linking.openURL(`https://wa.me/${order.user.phone}`)}
                        icon={'logo-whatsapp'}
                        bottom
                        color={'#25D366'}
                    >
                        Написать в WhatsApp
                    </StyledButton>
                ) : null
            ) : null}
            {fromProfile ? (
                <>
                    <StyledButton loading={toggling} onPress={handleToggleActive}>
                        {order.status === 'inactive' ? 'Повторить заказ' : 'Завершить заказ'}
                    </StyledButton>
                    <StyledButton
                        loading={deleting}
                        onPress={handleDelete}
                        style={{ marginBottom: 40 }}
                        icon={'ios-trash'}
                        bottom
                        color="#ed4337"
                    >
                        Удалить
                    </StyledButton>
                </>
            ) : null}
        </View>
    )
}

export default SingleOrder
