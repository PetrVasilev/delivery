import React, { useState } from 'react'
import { RefreshControl } from 'react-native'
import styled from 'styled-components'
import { useQuery } from '@apollo/react-hooks'
import { Ionicons } from '@expo/vector-icons'

import Order from '../components/Order'
import { GET_ORDERS } from '../gqls/order/queries'
import colors from '../utils/colors'

const Container = styled.ScrollView`
    flex: 1;
`

const Empty = styled.View`
    flex: 1;
    height: 100%;
    justify-content: center;
    align-items: center;
`

const EmptyText = styled.Text`
    font-size: 16px;
    color: gray;
    margin-top: 0;
`

const limit = 20

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1
}

const Orders = ({ navigation }) => {
    const [firstLoading, setFirstLoading] = useState(true)

    const { data, refetch, fetchMore } = useQuery(GET_ORDERS, {
        notifyOnNetworkStatusChange: true,
        fetchPolicy: 'network-only',
        pollInterval: 5000,
        onCompleted: () => setFirstLoading(false),
        onError: () => setFirstLoading(false),
        variables: {
            limit,
            skip: 0
        }
    })

    const getMoreOrders = () => {
        fetchMore({
            variables: {
                limit,
                skip: data ? (data.orders ? data.orders.length : 0) : 0
            },
            updateQuery: (prev, { fetchMoreResult }) => {
                if (!fetchMoreResult) return prev
                return {
                    orders: [
                        ...prev.orders,
                        ...fetchMoreResult.orders.filter(item => {
                            const index = prev.orders.findIndex(it => it._id === item._id)
                            if (index > -1) {
                                return false
                            } else {
                                return true
                            }
                        })
                    ]
                }
            }
        })
    }

    const orders = data ? data.orders.filter(item => item.status === 'active' && !item.deleted) : []

    return (
        <Container
            contentContainerStyle={{
                height: orders.length === 0 ? '100%' : null,
                paddingVertical: 15
            }}
            onScroll={({ nativeEvent }) => {
                if (isCloseToBottom(nativeEvent)) {
                    getMoreOrders()
                }
            }}
            scrollEventThrottle={400}
            refreshControl={
                <RefreshControl
                    refreshing={firstLoading}
                    onRefresh={async () => {
                        setFirstLoading(true)
                        await refetch()
                    }}
                />
            }
        >
            {orders.length === 0 && !firstLoading ? (
                <Empty>
                    <Ionicons name="ios-qr-scanner" color={colors.primary} size={80} />
                    <EmptyText>Заказов пока нет, но они будут :)</EmptyText>
                </Empty>
            ) : (
                orders.map((order, index) => (
                    <Order navigation={navigation} key={order._id} index={index} order={order} />
                ))
            )}
        </Container>
    )
}

export default Orders
