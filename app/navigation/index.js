import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import { Ionicons } from '@expo/vector-icons'

import colors from '../utils/colors'

import CreateOrder from '../screens/create-order'
import Profile from '../screens/profile'

import Orders from '../screens/orders'
import SingleOrder from '../screens/single-order'

const Tab = createBottomTabNavigator()

const getNavigationItemIcon = route => ({ focused, color, size }) => {
    let iconName
    let iconColor = focused ? colors.primary : color
    switch (route.name) {
        case 'CreateOrderStack':
            iconName = 'ios-add-circle'
            break
        case 'OrdersStack':
            iconName = 'ios-list-box'
            break
        case 'ProfileStack':
            iconName = 'md-person'
            break
        default:
            break
    }
    return <Ionicons name={iconName} size={size} color={iconColor} />
}

const OrdersStack = createStackNavigator()

const OrdersStackScreen = () => {
    return (
        <OrdersStack.Navigator>
            <OrdersStack.Screen
                name="Orders"
                component={Orders}
                options={{ title: 'Список заказов' }}
            />
            <OrdersStack.Screen
                name="SingleOrder"
                component={SingleOrder}
                options={{ title: 'Заказ', headerBackTitleVisible: false }}
            />
        </OrdersStack.Navigator>
    )
}

const CreateOrderStack = createStackNavigator()

const CreateOrderStackScreen = () => {
    return (
        <CreateOrderStack.Navigator>
            <CreateOrderStack.Screen
                name="CreateOrder"
                component={CreateOrder}
                options={{ title: 'Создать заказ' }}
            />
        </CreateOrderStack.Navigator>
    )
}

const ProfileStack = createStackNavigator()

const ProfileStackScreen = () => {
    return (
        <ProfileStack.Navigator>
            <ProfileStack.Screen
                name="Profile"
                component={Profile}
                options={{ title: 'Профиль' }}
            />
            <OrdersStack.Screen
                name="SingleOrder"
                component={SingleOrder}
                options={{ title: 'Заказ', headerBackTitleVisible: false }}
            />
        </ProfileStack.Navigator>
    )
}

const AuthorizedNavigation = () => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                showLabel: false
            }}
            screenOptions={({ route }) => ({
                tabBarIcon: getNavigationItemIcon(route)
            })}
        >
            <Tab.Screen name="CreateOrderStack" component={CreateOrderStackScreen} />
            {/* <Tab.Screen name="OrdersStack" component={OrdersStackScreen} /> */}
            <Tab.Screen name="ProfileStack" component={ProfileStackScreen} />
        </Tab.Navigator>
    )
}

export default AuthorizedNavigation
