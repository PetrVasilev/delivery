import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'

export default async function registerForPushNotificationsAsync() {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)

    if (status !== 'granted') {
        return null
    }

    let token = await Notifications.getExpoPushTokenAsync()

    return token
}
