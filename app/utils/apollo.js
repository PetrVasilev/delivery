import { AsyncStorage } from 'react-native'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { createHttpLink } from 'apollo-link-http'

import { showToast } from '../components/Toast'

const devHost = 'http://localhost:3000'
// const devHost = 'http://188.93.210.239'

export const host = process.env.NODE_ENV === 'development' ? devHost : 'http://188.93.210.239'

const GraphQL = `${host}/graphql`

const authLink = setContext(async (_, { headers }) => {
    const token = await AsyncStorage.getItem('token')
    return {
        headers: {
            ...headers,
            authorization: token ? token : ''
        }
    }
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
            console.log(
                `[GraphQL Error]: Message: ${message}, Location: ${locations}, Path: ${path}`
            )
        )
    if (networkError) {
        showToast('Нет подключения к серверу. Проверьте доступ к интернету')
    }
})

const httpLink = createHttpLink({
    uri: GraphQL,
    credentials: 'same-origin'
})

const link = ApolloLink.from([authLink, errorLink, httpLink])

const client = new ApolloClient({
    link,
    cache: new InMemoryCache()
})

export default client
