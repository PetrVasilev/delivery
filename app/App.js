import React, { useState } from 'react'
import { SafeAreaView, StatusBar } from 'react-native'
import { ApolloProvider, useQuery } from '@apollo/react-hooks'
import { AppLoading } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import AuthorizedNavigation from './navigation'

import LoginScreen from './screens/login'
import SetUserInfoScreen from './screens/set-user-info'

import { GET_USER } from './gqls/user/queries'
import Toast from './components/Toast'
import apollo from './utils/apollo'

const Stack = createStackNavigator()

const Application = () => {
    const { loading, data } = useQuery(GET_USER)
    const [isNotReady, setIsNotReady] = useState(true)

    if (loading || isNotReady) {
        return (
            <AppLoading
                startAsync={loadResourcesAsync}
                onError={error => console.warn(error)}
                onFinish={() => setIsNotReady(false)}
            />
        )
    }

    const logged = data ? (data._user ? true : false) : false

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            {logged ? (
                <Stack.Screen name="Home" component={AuthorizedNavigation} />
            ) : (
                <>
                    <Stack.Screen
                        name="Login"
                        component={LoginScreen}
                        options={{
                            animationTypeForReplace: !logged ? 'pop' : 'push'
                        }}
                    />
                    <Stack.Screen
                        name="SetUserInfo"
                        component={SetUserInfoScreen}
                        options={{
                            animationTypeForReplace: !logged ? 'pop' : 'push'
                        }}
                    />
                </>
            )}
        </Stack.Navigator>
    )
}

const loadResourcesAsync = async () => {
    await Promise.all([
        Asset.loadAsync([require('./assets/logo.png')]),
        Font.loadAsync({
            ...Ionicons.font
        })
    ])
}

export default function App() {
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ApolloProvider client={apollo}>
                <NavigationContainer>
                    <StatusBar barStyle="dark-content" backgroundColor="white" />
                    <Application />
                    <Toast />
                </NavigationContainer>
            </ApolloProvider>
        </SafeAreaView>
    )
}
