import React from 'react'
import styled from 'styled-components'
import { Form as AntForm, Input, Button, message } from 'antd'
import { useMutation } from '@apollo/react-hooks'

import Top from '../components/Top'
import { EDIT_PASSWORD } from '../gqls/admin/mutations'

const rules = {
    required: {
        required: true,
        message: 'Обязательное поле'
    }
}

const Form = styled(AntForm)`
    max-width: 400px;
    margin-top: 10px;
`

const Password = () => {
    const [editPassword, { loading }] = useMutation(EDIT_PASSWORD, {
        onCompleted: () => {
            message.success('Пароль успешно изменен')
        },
        onError: err => {
            let passwordErr = false
            err.graphQLErrors.forEach(item => {
                if (item.message === 'auth-error') {
                    passwordErr = true
                }
            })
            if (passwordErr) {
                message.error('Неправильный текущий пароль')
            } else {
                message.error('Не удалось изменить пароль')
            }
        }
    })

    const onSubmitForm = values => {
        editPassword({
            variables: {
                currentPassword: values.currentPassword,
                newPassword: values.newPassword
            }
        })
    }

    return (
        <>
            <Top title="Изменить пароль" />

            <Form
                onFinish={onSubmitForm}
                layout="vertical"
                name="change-password"
                initialValues={{
                    remember: true
                }}
            >
                <Form.Item label="Текущий пароль" name="currentPassword" rules={[rules.required]}>
                    <Input.Password />
                </Form.Item>
                <Form.Item label="Новый пароль" name="newPassword" rules={[rules.required]}>
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    label="Подтвердите пароль"
                    name="confirmPassword"
                    rules={[
                        rules.required,
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('newPassword') === value) {
                                    return Promise.resolve()
                                }
                                return Promise.reject('Новый пароль не совпадает')
                            }
                        })
                    ]}
                    dependencies={['newPassword']}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item>
                    <Button loading={loading} htmlType="submit" type="primary">
                        Отправить
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default Password
