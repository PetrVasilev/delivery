import React from 'react'
import { Form as AntForm, Button, Input, message } from 'antd'
import styled from 'styled-components'
import { useMutation } from '@apollo/react-hooks'

import Top from '../components/Top'
import { CREATE_MAILING } from '../gqls/admin/mutations'

const Form = styled(AntForm)`
    max-width: 400px;
    margin-top: 10px;
`

const Mailing = () => {
    const [createMailing, { loading }] = useMutation(CREATE_MAILING, {
        onCompleted: () => {
            message.success('Рассылка отправлена')
        },
        onError: () => {
            message.error('Не удалось отправить рассылку')
        }
    })

    const handleSubmit = values => createMailing({ variables: { text: values.text } })

    return (
        <>
            <Top title="Рассылка" helpText="Push - уведомление для всех пользователей" />

            <Form onFinish={handleSubmit} layout="vertical" name="mailing-form">
                <Form.Item
                    label="Текст рассылки"
                    name="text"
                    rules={[{ required: true, message: 'Обязательное поле' }]}
                >
                    <Input.TextArea />
                </Form.Item>

                <Form.Item>
                    <Button loading={loading} htmlType="submit" type="primary">
                        Отправить
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default Mailing
