import React from 'react'
import { Button as AntButton } from 'antd'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

import Top from '../components/Top'

const Button = styled(AntButton)`
    margin-top: 15px;
`

const Main = () => {
    return (
        <>
            <Top title="Главная" helpText="Это панель администратора Delivery" />

            <Link to="/password">
                <Button type="primary" ghost>
                    Изменить пароль
                </Button>
            </Link>
        </>
    )
}

export default Main
