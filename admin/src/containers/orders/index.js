import React, { useState } from 'react'
import styled from 'styled-components'
import { Table as AntTable, Tag, Button, Popconfirm, message } from 'antd'
import moment from 'moment'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { CheckOutlined, CloseOutlined } from '@ant-design/icons'

import Top from '../../components/Top'
import { GET_ALL_ORDERS } from '../../gqls/order/queries'
import { DELETE_ORDER, TOGGLE_ACTIVE_ORDER } from '../../gqls/order/mutations'

const Table = styled(AntTable)`
    margin-top: 20px;
`

const Expanded = styled.div`
    .item {
        .title {
            margin-bottom: 7px;
            font-style: italic;
        }
    }

    button {
        margin-top: 10px;
    }
`

const limit = 50

const Orders = () => {
    const [currentPage, setCurrentPage] = useState(1)

    const { data, loading, refetch } = useQuery(GET_ALL_ORDERS, {
        fetchPolicy: 'network-only',
        variables: {
            skip: 0,
            limit
        }
    })

    const handleTableChange = (pagination) => {
        const current = pagination.current
        setCurrentPage(current)
        refetch({
            skip: (current - 1) * limit,
            limit
        })
    }

    const orders = data ? data.allOrders : []
    const total = data ? data.totalOrders : 0

    return (
        <>
            <Top title="Заказы" />
            <Table
                loading={loading}
                rowKey={(obj) => obj._id}
                columns={columns}
                pagination={{
                    pageSize: limit,
                    total,
                    current: currentPage
                }}
                onChange={handleTableChange}
                dataSource={orders}
                expandable={{
                    expandedRowRender: (record) => <ExpandedInfo refetch={refetch} order={record} />
                }}
                scroll={{ x: 900 }}
                size={window.innerWidth < 500 ? 'small' : 'default'}
            />
        </>
    )
}

const ExpandedInfo = ({ order, refetch }) => {
    const [deleteOrder, { loading }] = useMutation(DELETE_ORDER, {
        onCompleted: () => {
            message.success('Заказ удален')
            refetch()
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось удалить заказ')
        }
    })
    const [toggleActiveOrder, { loading: toggling }] = useMutation(TOGGLE_ACTIVE_ORDER, {
        onCompleted: () => {
            message.success('Статус изменен')
        },
        onError: (err) => {
            console.error(err)
            message.error('Не удалось изменить статус')
        }
    })

    const handleDelete = () => {
        deleteOrder({
            variables: {
                _id: order._id
            }
        })
    }

    return (
        <Expanded>
            <div className="item">
                <div className="title">Продукты:</div>
                <ul>
                    {order.products.map((item, index) => (
                        <li key={index}>{item}</li>
                    ))}
                </ul>
            </div>
            {order.comment && (
                <div className="item">
                    <div className="title">Комментарий:</div>
                    <div className="value">{order.comment}</div>
                </div>
            )}
            <Button
                size="small"
                style={{ marginRight: 10 }}
                loading={toggling}
                type="primary"
                ghost
                onClick={() => {
                    toggleActiveOrder({
                        variables: {
                            _id: order._id
                        }
                    })
                }}
            >
                {order.status === 'active' ? `Завершить` : 'Сделать активным'}
            </Button>
            <Popconfirm title="Удалить?" onConfirm={handleDelete}>
                <Button size="small" loading={loading} type="danger" ghost>
                    Удалить
                </Button>
            </Popconfirm>
        </Expanded>
    )
}

const columns = [
    {
        title: 'Адрес',
        dataIndex: 'address',
        key: 'address'
    },
    {
        title: 'Пользователь',
        dataIndex: 'user',
        key: 'user',
        render: (user) => `${user.name}, ${user.phone}`
    },
    {
        title: 'Статус',
        dataIndex: 'status',
        key: 'status',
        render: (status) => getStatus(status)
    },
    {
        title: 'Удален',
        dataIndex: 'deleted',
        key: 'deleted',
        render: (deleted) =>
            deleted ? (
                <CheckOutlined style={{ color: 'green', fontSize: 16 }} />
            ) : (
                <CloseOutlined style={{ color: 'red', fontSize: 16 }} />
            )
    },
    {
        title: 'Создан',
        dataIndex: 'created',
        key: 'created',
        render: (created) => moment(created).format('DD.MM.YYYY HH:mm')
    }
]

const getStatus = (status) => {
    switch (status) {
        case 'active':
            return <Tag color="green">Активный</Tag>
        case 'inactive':
            return <Tag color="red">Не активный</Tag>
        default:
            return null
    }
}

export default Orders
