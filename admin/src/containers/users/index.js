import React, { useState } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { Table as AntTable } from 'antd'
import styled from 'styled-components'
import moment from 'moment'

import Top from '../../components/Top'
import { GET_ALL_USER } from '../../gqls/user/queries'

const Table = styled(AntTable)`
    margin-top: 20px;
`

const limit = 50

const Users = () => {
    const [currentPage, setCurrentPage] = useState(1)

    const { data, loading, refetch } = useQuery(GET_ALL_USER, {
        fetchPolicy: 'network-only',
        variables: {
            skip: 0,
            limit
        }
    })

    const users = data ? data.users : []
    const total = data ? data.usersTotal : 0

    const handleTableChange = pagination => {
        const current = pagination.current
        setCurrentPage(current)
        refetch({
            skip: (current - 1) * limit,
            limit
        })
    }

    return (
        <>
            <Top title="Пользователи" />

            <Table
                loading={loading}
                pagination={{
                    pageSize: limit,
                    total,
                    current: currentPage
                }}
                rowKey={obj => obj._id}
                columns={columns}
                dataSource={users}
                onChange={handleTableChange}
                scroll={{ x: 900 }}
                size={window.innerWidth < 500 ? 'small' : 'default'}
            />
        </>
    )
}

const columns = [
    {
        title: 'Имя',
        dataIndex: 'name',
        key: 'name'
    },
    {
        title: 'Номер телефона',
        dataIndex: 'phone',
        key: 'phone'
    },
    {
        title: 'Адрес',
        dataIndex: 'address',
        key: 'address'
    },
    {
        title: 'Дата регистрации',
        dataIndex: 'created',
        key: 'created',
        render: created => moment(created).format('DD.MM.YYYY HH:mm')
    }
]

export default Users
