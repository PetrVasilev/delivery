import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { onError } from 'apollo-link-error'
import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { createUploadLink } from 'apollo-upload-client'

export const host = 'http://localhost:3000'

const GraphQL = process.env.NODE_ENV === 'production' ? '/graphql' : `${host}/graphql`

const authLink = setContext(async (_, { headers }) => {
    const token = await localStorage.getItem('token')
    return {
        headers: {
            ...headers,
            authorization: token ? token : ''
        }
    }
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map(({ message, locations, path }) =>
            console.error(
                `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
            )
        )
    if (networkError) console.error(`[Network error]: ${networkError}`)
})

const uploadLink = createUploadLink({
    uri: GraphQL,
    credentials: 'same-origin'
})

const link = ApolloLink.from([authLink, errorLink, uploadLink])

const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
    defaultHttpLink: false,
    defaultFetchPolicy: 'network-only'
})

export default client
