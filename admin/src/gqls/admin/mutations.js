import gql from 'graphql-tag'

export const LOGIN_ADMIN = gql`
    mutation($login: String!, $password: String!) {
        loginAdmin(data: { login: $login, password: $password }) {
            token
            admin {
                _id
                name
                login
            }
        }
    }
`

export const EDIT_PASSWORD = gql`
    mutation($currentPassword: String!, $newPassword: String!) {
        editPasswordAdmin(data: { currentPassword: $currentPassword, newPassword: $newPassword }) {
            _id
        }
    }
`

export const CREATE_MAILING = gql`
    mutation($text: String!) {
        createMailing(data: { text: $text })
    }
`
