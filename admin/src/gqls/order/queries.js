import gql from 'graphql-tag'

export const GET_ALL_ORDERS = gql`
    query($limit: Int, $skip: Int) {
        allOrders(limit: $limit, skip: $skip) {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            whatsapp
            created
            status
            deleted
        }

        totalOrders
    }
`
