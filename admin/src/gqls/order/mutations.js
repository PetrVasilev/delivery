import gql from 'graphql-tag'

export const DELETE_ORDER = gql`
    mutation($_id: String!) {
        deleteOrderAdmin(where: { _id: $_id }) {
            _id
        }
    }
`

export const TOGGLE_ACTIVE_ORDER = gql`
    mutation($_id: String!) {
        toggleActiveOrder(where: { _id: $_id }) {
            _id
            address
            products
            user {
                _id
                name
                phone
            }
            comment
            whatsapp
            created
            status
            deleted
        }
    }
`
