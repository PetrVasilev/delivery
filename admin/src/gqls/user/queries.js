import gql from 'graphql-tag'

export const GET_ALL_USER = gql`
    query($limit: Int, $skip: Int) {
        users(limit: $limit, skip: $skip) {
            _id
            name
            phone
            address
            created
        }

        usersTotal
    }
`
