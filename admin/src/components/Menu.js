import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Menu } from 'antd'
import { UnorderedListOutlined, UserOutlined, NotificationOutlined } from '@ant-design/icons'

const MenuComponent = () => {
    const { pathname } = window.location
    return (
        <Menu theme="dark" mode="inline" defaultSelectedKeys={[pathname]}>
            <Menu.Item style={{ marginTop: 7 }} key="/orders">
                <MenuLink to="/orders">
                    <UnorderedListOutlined />
                    Заказы
                </MenuLink>
            </Menu.Item>
            <Menu.Item key="/users">
                <MenuLink to="/users">
                    <UserOutlined />
                    Пользователи
                </MenuLink>
            </Menu.Item>
            <Menu.Item key="/mailing">
                <MenuLink to="/mailing">
                    <NotificationOutlined />
                    Рассылка
                </MenuLink>
            </Menu.Item>
        </Menu>
    )
}

const MenuLink = styled(Link)`
    display: flex;
    flex-direction: row;
    align-items: center;

    a {
        color: white;
    }
`

export default MenuComponent
