import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { createGlobalStyle } from 'styled-components'
import { ConfigProvider } from 'antd'
import { ApolloProvider } from '@apollo/react-hooks'
import ru from 'antd/es/locale/ru_RU'
import 'antd/dist/antd.css'
import 'moment/locale/ru'

import * as serviceWorker from './serviceWorker'
import withLayout from './components/Layout'
import apollo from './utils/apollo'

import Main from './containers/main'
import Login from './containers/login'
import Password from './containers/password'
import Mailing from './containers/mailing'

import Orders from './containers/orders'
import Users from './containers/users'

const GlobalStyles = createGlobalStyle`
    body {
        margin: 0;
        padding: 0;
        background: whitesmoke;
    }

    .center {
        width: 100%;
        height: 60vh;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .ant-layout-sider {
        z-index: 1;
    }
`

const App = () => {
    return (
        <ApolloProvider client={apollo}>
            <ConfigProvider locale={ru}>
                <Router>
                    <Switch>
                        <Route path="/" exact component={props => withLayout(props, Main)} />
                        <Route
                            path="/password"
                            exact
                            component={props => withLayout(props, Password)}
                        />
                        <Route
                            path="/orders"
                            exact
                            component={props => withLayout(props, Orders)}
                        />
                        <Route path="/users" exact component={props => withLayout(props, Users)} />
                        <Route
                            path="/mailing"
                            exact
                            component={props => withLayout(props, Mailing)}
                        />
                        <Route path="/login" exact component={Login} />
                    </Switch>
                </Router>
                <GlobalStyles />
            </ConfigProvider>
        </ApolloProvider>
    )
}

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.unregister()
