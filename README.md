Before run server, add **.env** file in **server/.env**:

```
MONGO_URL=mongodb://localhost:27017/delivery
PORT=3000
ADMIN_SECRET=admin_secret
ADMIN_LOGIN=admin
ADMIN_PASSWORD=admin
USER_SECRET=user_secret
SMS_KEY=7a230a33-3c9d-6624-153a-415c410ab2ba
```